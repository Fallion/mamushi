# Mamushi

[![Build Status](https://cloud.drone.io/api/badges/fallion/mamushi/status.svg)](https://cloud.drone.io/fallion/mamushi)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6e45aac0af1b4cd3a9ba77eb9a2520d0)](https://app.codacy.com/app/simon_26/mamushi?utm_source=github.com&utm_medium=referral&utm_content=Fallion/mamushi&utm_campaign=Badge_Grade_Dashboard)
[![codecov](https://codecov.io/gh/Fallion/mamushi/branch/master/graph/badge.svg)](https://codecov.io/gh/Fallion/mamushi)

Port of [https://github.com/spf13/viper](https://github.com/spf13/viper). More info coming soon.
