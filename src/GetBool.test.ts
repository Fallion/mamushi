import test from "ava";
import { GetBool } from "./GetBool";

test("GetBool happy path", t => {
  process.env.TEST_STRING = "Hi";
  t.is(GetBool("TEST_STRING"), true);
});

test("GetBool on int", t => {
  // @ts-ignore
  process.env.TEST_STRING = 123;
  t.is(GetBool("TEST_STRING"), true);
});

test("GetBool with missing env", t => {
  delete process.env.TEST_STRING;
  t.is(GetBool("TEST_STRING"), false);
});

test("GetBool === false", t => {
  process.env.TEST_STRING = "false"
  t.is(GetBool("TEST_STRING"), false);
});

test("GetBool === true", t => {
  process.env.TEST_STRING = "true"
  t.is(GetBool("TEST_STRING"), true);
});
