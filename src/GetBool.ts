export const GetBool = (key: string): boolean => {
  if (!process.env[key]) {
    return false;
  }
  if (process.env[key] === "true") {
    return true
  }
  if (process.env[key] === "false") {
    return false
  }
  return Boolean(process.env[key]);
};
