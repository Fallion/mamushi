import test from "ava";
import { GetString } from "./GetString";

test("GetString happy path", t => {
  process.env.TEST_STRING = "Hi";
  t.is(GetString("TEST_STRING"), "Hi");

  // @ts-ignore
  process.env.TEST_STRING = 123;
  t.is(GetString("TEST_STRING"), "123");
});

test("GetString with missing env", t => {
  delete process.env.TEST_STRING;
  t.is(GetString("TEST_STRING"), "");
});
