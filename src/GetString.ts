export const GetString = (key: string): string => {
  if (!process.env[key]) {
    return "";
  }
  return String(process.env[key]);
};
